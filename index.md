# iCE Bling

This is an experimental project by Electronut Labs.

![iCE Bling](ice-bling-sm.jpg)

## What is iCE Bling ? 

iCE Bling is an Lattice iCE40UP5k FPGA based LED earrings. The earrings have 8x8 grid LEDs and are powered by CR2032 coin cell.

## The Design of iCE Bling

Here is link to the blog, [iCE Bling – Making LED Earrings with an FPGA](https://electronut.in/ice-bling-making-led-earrings-with-an-fpga/), on our website which will give you detailed information about iCE Bling.

## Code Repository 

You can find the design files and code for the iCE Bling here.

https://gitlab.com/electronutlabs-public/ice-bling

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).
