/*

    letters88.v

    Scrolling letters on an 8x8 grid.

*/

`default_nettype none

module letters88 (
    input resetn,
    input clk,
    output [63:0] fb
);

// buffers to hold letters
reg [63:0] r_fb1;
reg [63:0] r_fb2;

// 1/0 select letter
reg r_sel;

// counters
reg [21:0] scroll_counter;
reg [24:0] sw_counter;

// assign letter
assign fb = r_sel ? r_fb1 : r_fb2;

always @ (posedge clk)
    begin
        
        if (!resetn)
            begin
                // "H"
                r_fb1[0:7]   = 8'b01000010;    
                r_fb1[8:15]  = 8'b01000010; 
                r_fb1[16:23] = 8'b01000010; 
                r_fb1[24:31] = 8'b01111110; 
                r_fb1[32:39] = 8'b01111110; 
                r_fb1[40:47] = 8'b01000010; 
                r_fb1[48:55] = 8'b01000010; 
                r_fb1[56:63] = 8'b01000010; 
                
                // "M"
                r_fb2[0:7]   = 8'b01000010;    
                r_fb2[8:15]  = 8'b01000010; 
                r_fb2[16:23] = 8'b01000010; 
                r_fb2[24:31] = 8'b01000010; 
                r_fb2[32:39] = 8'b01011010; 
                r_fb2[40:47] = 8'b01011010; 
                r_fb2[48:55] = 8'b01100110; 
                r_fb2[56:63] = 8'b01000010; 

                // reset vars
                scroll_counter <= 0;
                r_sel <= 0;
                sw_counter <= 0;
            end 

        // increment counters
        scroll_counter <= scroll_counter + 1;
        sw_counter <= sw_counter + 1;

        // change letter selection
        if (!sw_counter)
            r_sel <= ~r_sel;

        if (!scroll_counter)
            begin
                // scroll letter 1 
                r_fb1[0:7]   <= {r_fb1[6:0],  r_fb1[7]};
                r_fb1[8:15]  <= {r_fb1[14:8], r_fb1[15]};
                r_fb1[16:23] <= {r_fb1[22:16],r_fb1[23]};
                r_fb1[24:31] <= {r_fb1[30:24], r_fb1[31]};
                r_fb1[32:39] <= {r_fb1[38:32], r_fb1[39]};
                r_fb1[40:47] <= {r_fb1[46:40], r_fb1[47]};
                r_fb1[48:55] <= {r_fb1[54:48], r_fb1[55]};
                r_fb1[56:63] <= {r_fb1[62:56], r_fb1[63]};

                // scroll letter 2
                r_fb2[0:7]   <= {r_fb2[6:0],  r_fb2[7]};
                r_fb2[8:15]  <= {r_fb2[14:8], r_fb2[15]};
                r_fb2[16:23] <= {r_fb2[22:16],r_fb2[23]};
                r_fb2[24:31] <= {r_fb2[30:24], r_fb2[31]};
                r_fb2[32:39] <= {r_fb2[38:32], r_fb2[39]};
                r_fb2[40:47] <= {r_fb2[46:40], r_fb2[47]};
                r_fb2[48:55] <= {r_fb2[54:48], r_fb2[55]};
                r_fb2[56:63] <= {r_fb2[62:56], r_fb2[63]};
            end

    end


endmodule
